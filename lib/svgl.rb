require 'nxml'

class SVGL < NXML

  def initialize(&block)
    super do
      self.standalone = false
      doctype.pubref "-//W3C//DTD SVG 1.1//EN", "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"
      root :svg do
        self.xmlns = "http://www.w3.org/2000/svg"
        self.version = "1.1"

        instance_eval &block if block_given?
      end
    end
  end

end
