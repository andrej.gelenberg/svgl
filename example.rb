require 'svgl'

def u(x)
  "#{x}mm"
end

svg = SVGL.new do
  self.version = '1.1'
  self.baseProfile = 'none'

  self.x = u 0
  self.y = u 0

  self.width  = u 10
  self.height = u 10

  self.contentScriptType = "application/ecmascript"
  self.contentStyleType  = "text/css"

  self.zoomAndPan = :magnify

  tag :title do 
    text "yay, title"
  end

  tag :desc  do
    self.class = "foo"
    self.style = "meh"

    text "this is a demonstration of the libraries capabilities"
  end

  self.xml_base = "basetest"

  tag :g do
    self.id = 'ziemlich'

    tag :svg do
      self.id = "test"
    end
  end
end

puts svg.to_str
